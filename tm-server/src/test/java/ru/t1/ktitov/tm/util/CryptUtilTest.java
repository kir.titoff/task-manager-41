package ru.t1.ktitov.tm.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class CryptUtilTest {

    @Test
    public void encrypt() {
        Assert.assertEquals("bd9sjB3Arw+kMXYamSPlRw==", CryptUtil.encrypt("123", "HELLO"));
    }

    @Test
    public void decrypt() {
        Assert.assertEquals("HELLO", CryptUtil.decrypt("123", "bd9sjB3Arw+kMXYamSPlRw=="));
    }

}
