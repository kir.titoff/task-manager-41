package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    ProjectDTO removeProjectById(@Nullable String userId, @Nullable String projectId);

}
