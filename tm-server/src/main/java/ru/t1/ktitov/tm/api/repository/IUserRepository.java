package ru.t1.ktitov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, fst_name, last_name, mid_name, " +
            "email, role, locked) VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, " +
            "#{lastName}, #{middleName}, #{email}, #{role}, #{locked})")
    void addUser(@NotNull final UserDTO user);

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "fst_name = #{firstName}, last_name = #{lastName}, mid_name = #{middleName}, " +
            "role = #{role}, locked = #{locked} WHERE id = #{id}")
    void updateUser(@Nullable final UserDTO user);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findUserByLogin(@Nullable final String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findUserByEmail(@Nullable final String email);

    @Select("DELETE FROM tm_user WHERE id = #{id}")
    void deleteUser(@Nullable final String id);

    @Delete("TRUNCATE TABLE tm_user")
    void clearUsers();

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "passwordHash", column = "password")
    })
    List<UserDTO> findAllUsers();

    @Select("SELECT * FROM tm_user WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findUserById(@Nullable final String id);

    @Select("SELECT COUNT(1) FROM tm_user")
    int getUsersSize();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeUser(@Nullable final UserDTO user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeUserById(@NotNull final String id);

}
